/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package holamundopoo;

/**
 *
 * @author USUARIO
 */
public class Circulo {
    
    private double Radio;
    
    private double Pi;

    public Circulo() {
        this.Pi = 3.1416;
    }

    public double getRadio() {
        return Radio;
    }

    public void setRadio(double Radio) {
        this.Radio = Radio;
    }

    public double getPi() {
        return Pi;
    }

    //SOLID
    //Metodo que calcula el area de un circulo .......
    public  double CalcularArea()
    {
        return Radio * Pi;
    }
    
}
